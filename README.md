# Django keybase REST and GraphQL integrations

This is a proof of concept project with the setup of keybase JWT authentication in a REST and GraphQL api built with django

## requirements
  *  

## Configure Environment
In order to configure the environment follow these steps in order:

### Keybase
  1. Navigate to `localhost:8080/auth/admin`

  2. Create a new realm "myrealm"

  3. Create the client for the frontend
    ClientID: frontend
    Root URL: http://localhost:3000/
  
  4. Create the client for the REST API
    ClientID: rest_api
    Root URL: http://localhost:8000/
    after creation change the access type to bearer-only
    
  5. Edit the frontend client and create a mapper to the rest-api audience: 
     name: rest-api audience
     mapper type: Audience
     Included Client Audience: rest-api
     
  6. Create the test user
     name: test_user
     email: test.user@myproject.com 
     first name: test
     last name: user
     
     after creating the user, navigate to the credentials menu and set a temporary password
     
    
### Api

  1. run migrations: `docker-compose run --rm api python3 manage.py migrate`
  
  2. load fixtures: `docker-compose run --rm api python3 manage.py loaddata fixtures.json`

  3. Use django admin to configure a new Keycloak server `localhost:8000/admin/server`
    url: http://localhost:8080/
    internal url: http://keycloak:8080/ <- this is important because we are using docker-compose

  4. Use django admin configure a new Keycloak realm `localhost:8000/admin/realm/`
    name: myrealm
    server: pick the previously created server
    clientID:
      name: rest-api
      secret: (copy from the keycloak admin panel->clients->rest-api->credentials)
  
  5. Refresh OpenID Connect.well-known (realm listing action)
  
  6. Refresh the certificates (JWT decoding)
    
    
### Frontend
  (TODO: reference the frontend sample project)
  
  1. Navigate to the `localhost:8080/auth/admin` -> clients -> frontend -> instalation, select the Keycloak OIDC JSON format and copy its contents to `keycloak-react-client/public/keycloak.json`
  
  2. restart docker-compose


## Testing Execution
(TODO: user insomnia collection for graphql)

  Navigate to `localhost:3000` and click on `secured component` link, this redirects you to the keybase login panel, use the username and password that was set in the 6th step of the keybase configuration and set the passoword (remember that the password set previously was temporary). Now you should see information about the user and a JWT token, copy the token and paste it in the authorization header of the following request
  ```
  curl --request GET \
  --url http://localhost:8000/api/products \
  --header 'authorization: Bearer {{ paste token here }}'
  ```

# TODO
  * test django 3.1 async middleware
  * Authorization methods
