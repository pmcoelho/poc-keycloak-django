import React, {Component} from 'react'

class UserInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      id: '',
      jwt: this.props.keycloak.token
    }
    this.props.keycloak.loadUserInfo().then(userInfo => {
      console.log(this.props.keycloak)
      this.setState({
        name: userInfo.name,
        email: userInfo.email,
        id: userInfo.sub
      })
    })
  }

  render() {
    return (
      <div className="UserInfo">
        <p>Name: {this.state.name}</p>
        <p>Email: {this.state.email}</p>
        <p>ID: {this.state.id}</p>
        <p>jwt: {this.state.jwt}</p>
      </div>
    )
  }
}
export default UserInfo
