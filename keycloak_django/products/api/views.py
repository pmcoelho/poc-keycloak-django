from rest_framework import viewsets

from .serializers import ProductSerializer
from ..models import Product


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)
